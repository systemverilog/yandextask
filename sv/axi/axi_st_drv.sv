//------------------------------------------------------------------------------
//
// CLASS: drv_c
//
//------------------------------------------------------------------------------
class drv_c #(
		int DATA_SIZE  = 32,
		int ID_SIZE    = 8
	) extends uvm_driver #(axi_st_item#(DATA_SIZE));

	// The virtual interface used to drive and view HDL signals.
	protected virtual IAxiStream  #(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE))   vif;

	cfg_c          cfg;
	
	// Master Id
	protected int           master_id;
	
	rand bit 					t_ready;
	
	protected axi_st_item #(DATA_SIZE)     axi_st_trans;

	// Provide implmentations of virtual methods such as get_type_name and create
	`uvm_component_utils_begin(axi_st_pkg::drv_c #(DATA_SIZE))
		`uvm_field_object    (cfg,             UVM_DEFAULT | UVM_REFERENCE)
		`uvm_field_int       (master_id,       UVM_DEFAULT)
	`uvm_component_utils_end

	// new - constructor
	function new (string name, uvm_component parent);
		super.new(name, parent);
	endfunction : new

	function void build_phase(uvm_phase phase);
		uvm_tree_printer  tree_printer;

		super.build_phase(phase);
		if(!uvm_config_db#(virtual IAxiStream #(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE)))::get(this, "", "vif", vif))
			`uvm_fatal("NOVIF",{"virtual interface must be set for: ",get_full_name(),".vif"});

		if (cfg == null) begin
			if (!uvm_config_db#(cfg_c)::get(this, "", "cfg", cfg)) begin
				`uvm_info("NOCONFIG", {"No config for:", get_full_name()}, UVM_LOW)
			end
		end

		`uvm_info(get_type_name(),$sformatf("axi_st_drv :\n%s", cfg.sprint()), UVM_LOW)
	endfunction: build_phase

	// run phase
	virtual task run_phase(uvm_phase phase);
//    generate_err_map();
		fork
			get_and_drive();
			reset_signals();
			begin
				if (cfg.has_rdy_handler) begin
					ready_handler();
				end	
			end	
		//dbg_display_objections(phase);
		join
	endtask : run_phase

	// get_and_drive
	virtual protected task get_and_drive();
		@(posedge vif.areset_n);
		forever begin
			@(posedge vif.aclk);

			seq_item_port.get_next_item(req);
//        req.print();
			$cast(rsp, req.clone());
			rsp.set_id_info(req);
			drive_transfer(rsp);
			seq_item_port.item_done();
			seq_item_port.put_response(rsp);
		end
	endtask : get_and_drive


	// reset_signals
	virtual protected task reset_signals();
		forever begin
			@(negedge vif.areset_n);
			vif.t_data        <= {DATA_SIZE{1'b0}};
			vif.t_id       <= {ID_SIZE{1'b0}};
			vif.t_last     <= 1'b0;
			vif.t_valid       <= 1'b0;
		end
	endtask : reset_signals

	virtual protected task drive_transfer (axi_st_item#(DATA_SIZE) trans);
		`uvm_info(get_type_name(),$sformatf("axi_st_drv.trans :\n%s", trans.sprint()), UVM_LOW)

		if (trans.pre_delay > 0) begin
			repeat (trans.pre_delay) @(posedge vif.aclk);
		end
		
		vif.t_data                 <= trans.data[0];
		vif.t_id                   <= master_id;
		vif.t_valid                <= 1'b1;
		vif.t_last                 <= 1'b0;
		
		for (int idx=1; idx < trans.data.size-1; idx=idx+1) begin
			do begin
				@(posedge vif.aclk iff vif.areset_n === 1);
			end while (!vif.t_ready);
			vif.t_data                 <= trans.data[idx];
			vif.t_id                   <= master_id;
			vif.t_valid                <= 1'b1;
			vif.t_last                 <= 1'b0;
		end

		do begin
			@(posedge vif.aclk iff vif.areset_n === 1);
		end while (!vif.t_ready);
		vif.t_data                		<= trans.data[trans.data.size-1];
		vif.t_id                     	<= master_id;
		vif.t_valid                  	<= 1'b1;
		vif.t_last                		<= 1'b1;


		do begin
			@(posedge vif.aclk iff vif.areset_n === 1);
		end while (!vif.t_ready);
		vif.t_data                		<= '0;
		vif.t_id                     	<= '0;
		vif.t_valid                  	<= 1'b0;
		vif.t_last                		<= 1'b0;

		if (trans.post_delay > 0) begin
			repeat (trans.post_delay) @(posedge vif.aclk);
		end
	endtask : drive_transfer
	
	// reset_signals
  	virtual protected task ready_handler();
	    forever begin
	       @(posedge vif.aclk iff vif.areset_n === 1);
		    std::randomize(t_ready) with { t_ready dist {1 := 6, 0 := 4};};
		    vif.t_ready				<= t_ready;
	    end
  	endtask : ready_handler
	
endclass : drv_c




