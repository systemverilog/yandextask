//------------------------------------------------------------------------------
//
// CLASS: agent_c
//
//------------------------------------------------------------------------------
class agent_c #(
		int DATA_SIZE  = 32,
		int ID_SIZE    = 8
	) extends uvm_agent;

	protected virtual IAxiStream           #(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE))   vif;

	cfg_c cfg;

	drv_c       #(
		.DATA_SIZE(DATA_SIZE),
		.ID_SIZE(ID_SIZE)
	) drv;
	mon_c       #(
		.DATA_SIZE(DATA_SIZE),
		.ID_SIZE(ID_SIZE)
	) mon;

	uvm_sequencer#(axi_st_item#(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE)))   sqr;

	`uvm_component_utils_begin(axi_st_pkg::agent_c #(DATA_SIZE, ID_SIZE))
		`uvm_field_object(cfg, UVM_DEFAULT | UVM_REFERENCE)
	`uvm_component_utils_end

	// new - constructor
	function new (string name, uvm_component parent);
		super.new(name, parent);

		if(!uvm_config_db#(virtual IAxiStream#(.DATA_SIZE(DATA_SIZE), .ID_SIZE(ID_SIZE)))::get(this, "", "vif", vif))
			`uvm_fatal("NOVIF",{"virtual interface must be set for: ",get_full_name(),".vif"});

		uvm_config_db#(virtual IAxiStream#(.DATA_SIZE(DATA_SIZE), .ID_SIZE(ID_SIZE)))::set(this, "*", "vif", vif);

	endfunction : new

	// build_phase
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		//var1
		if (cfg == null) begin
			`uvm_info(get_type_name(),$sformatf("00000"), UVM_LOW)
			if (!uvm_config_db#(cfg_c)::get(this, "", "cfg", cfg)) begin
				cfg = cfg_c::type_id::create("cfg", this);
				`uvm_info(get_type_name(),$sformatf("1111"), UVM_LOW)
			end
		end
		`uvm_info(get_type_name(),$sformatf("2222"), UVM_LOW)
		uvm_config_db#(cfg_c)::set(this, "*", "cfg", cfg);

		`uvm_info(get_type_name(),$sformatf("axi_st_agent :\n%s", cfg.sprint()), UVM_LOW)

		if (cfg.has_mon) begin
			mon = mon_c#(
				.DATA_SIZE(DATA_SIZE),
				.ID_SIZE(ID_SIZE)
			)::type_id::create("mon", this);
		end
		
		if(get_is_active() == UVM_ACTIVE) begin
			sqr = uvm_sequencer#(axi_st_item#(DATA_SIZE))::type_id::create("sqr", this);
			drv = drv_c#(
				.DATA_SIZE(DATA_SIZE),
				.ID_SIZE(ID_SIZE)
			)::type_id::create("drv", this);
		end
	endfunction : build_phase

	// connect_phase
	function void connect_phase(uvm_phase phase);
		if(get_is_active() == UVM_ACTIVE) begin
			drv.seq_item_port.connect(sqr.seq_item_export);
		end
	endfunction : connect_phase

endclass : agent_c
