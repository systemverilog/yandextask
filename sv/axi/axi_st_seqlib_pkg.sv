// File:
// adc_seqlib_pkg.sv
// Author:
// Vetoshkin Alexey, aa.vetoshkin@systemverilog.ru

`include "uvm_macros.svh"

// package: axi_st_seqlib_pkg
package axi_st_seqlib_pkg;

   //----------------------------------------------------------------------------------------
   // Group: Imports
   import uvm_pkg::*;

   //----------------------------------------------------------------------------------------
   // Group: Includes

   `include "axi_st_seq_lib.sv"

endpackage : axi_st_seqlib_pkg


