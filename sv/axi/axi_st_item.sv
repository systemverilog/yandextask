//------------------------------------------------------------------------------
//
// CLASS: axi_st_item
//------------------------------------------------------------------------------
class axi_st_item #(int DATA_SIZE=32, int ID_SIZE 	= 8) extends uvm_sequence_item;

  string                    			block_name = "";
  rand bit [7:0]							data_size;	
  rand bit [DATA_SIZE-1:0]				data[];
  
  rand bit [ID_SIZE-1:0]				id;	
  rand bit 									valid;
  rand bit 									last;
  rand bit 									ready;
  rand int unsigned       				pre_delay = 0;
  rand int unsigned       				post_delay = 0;
  
  	constraint c_pre_delay {
		pre_delay > 5;
		pre_delay < 50;
	}

	constraint c_post_delay {
		post_delay < 1000;
	}
  
  	constraint data_size_ct { 
	  (data.size() == data_size); 
  	}
  
  `uvm_object_utils_begin (axi_st_item#(DATA_SIZE,ID_SIZE))
  	`uvm_field_string   		(block_name,        		UVM_DEFAULT | UVM_NOCOMPARE)
  	   `uvm_field_int      		(data_size,      		UVM_DEFAULT | UVM_DEC)
   	`uvm_field_array_int		(data,    				UVM_DEFAULT)   	
   	`uvm_field_int      		(id,      				UVM_DEFAULT)
   	`uvm_field_int      		(valid,      			UVM_DEFAULT)
   	`uvm_field_int      		(last,      			UVM_DEFAULT)
   	`uvm_field_int      		(ready,      			UVM_DEFAULT)
   	`uvm_field_int          (pre_delay,          UVM_DEFAULT | UVM_DEC)
		`uvm_field_int          (post_delay,         UVM_DEFAULT | UVM_DEC)
  `uvm_object_utils_end

  // new - constructor
  function new (string name = "axi_st_item_inst");
    super.new(name);
  endfunction : new

endclass : axi_st_item
