//------------------------------------------------------------------------------
//
// CLASS: cfg_c
//
//------------------------------------------------------------------------------
class cfg_c extends uvm_object;
	uvm_active_passive_enum is_active 	= UVM_ACTIVE;
	bit has_sb									= 0;
	bit has_mon									= 0;
	bit has_rdy_handler						= 0;
	bit print_collected_item				= 0;
	

  	`uvm_object_utils_begin(axi_st_pkg::cfg_c)
	    `uvm_field_enum			(uvm_active_passive_enum, is_active,	UVM_DEFAULT)
	    `uvm_field_int      	(has_sb,    									UVM_DEFAULT|UVM_DEC)
	    `uvm_field_int      	(has_mon,    									UVM_DEFAULT|UVM_DEC)
	    `uvm_field_int      	(has_rdy_handler,    						UVM_DEFAULT|UVM_DEC)
	    `uvm_field_int      	(print_collected_item,  					UVM_DEFAULT|UVM_DEC)
  	`uvm_object_utils_end

  	// new - constructor
  	function new (string name = "axi_st_cfg_inst");
    	super.new(name);
  	endfunction : new

endclass : cfg_c
