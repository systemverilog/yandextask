//------------------------------------------------------------------------------
//
// CLASS: mon_c
//
//------------------------------------------------------------------------------

class mon_c #(
		int DATA_SIZE 	= 32,
		int ID_SIZE 	= 8
	) extends uvm_monitor;

  	// The virtual interface used to view HDL signals.
  	protected virtual IAxiStream #(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE)) 	vif;

  	// Property indicating the number of transactions occuring on the ubus.
	protected int unsigned num_transactions = 0;

  	// Analysis ports for the item_collected.
  	uvm_analysis_port #(axi_st_item #(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE)) ) 		axi_st_item_collected_port;

	cfg_c				cfg;

  // Provide implementations of virtual methods such as get_type_name and create
  	`uvm_component_utils_begin (axi_st_pkg::mon_c #(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE)))
	  	`uvm_field_object(cfg, UVM_DEFAULT | UVM_REFERENCE)
  	`uvm_component_utils_end

  	// new - constructor
  	function new (string name, uvm_component parent);
	    super.new(name, parent);
	    axi_st_item_collected_port = new("axi_st_item_collected_port", this);
  	endfunction : new

  	function void build_phase(uvm_phase phase);
	  	super.build_phase(phase);
      	if(!uvm_config_db#(virtual IAxiStream#(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE)))::get(this, "", "vif", vif))
       		`uvm_fatal("NOVIF",{"virtual interface must be set for: ",get_full_name(),".vif"});

      	if (cfg == null) begin
		  	if (!uvm_config_db#(cfg_c)::get(this, "", "cfg", cfg)) begin
	  			`uvm_info("NOCONFIG", {"No axi_st config for:", get_full_name()}, UVM_LOW)
			end
	  	end

  	endfunction: build_phase

  	// run phase
  	task run_phase(uvm_phase phase);
	    fork
	      observe_reset();
	      collect_transactions();
	    join
  	endtask : run_phase

  	// observe_reset
  	task observe_reset();
	    fork
	      forever begin
	        @(negedge vif.areset_n);
	        `uvm_info(get_type_name(),$sformatf("RST_START"), UVM_LOW)
	      end
	      forever begin
	        @(posedge vif.areset_n);
	        `uvm_info(get_type_name(),$sformatf("RST_STOP"), UVM_LOW)
	      end
	    join
  	endtask : observe_reset

  	// collect_transactions
  	virtual protected task collect_transactions();
	  	axi_st_item #(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE)) 	axi_st_item_collected;
	  	fork
		    forever begin
		    	@(posedge vif.aclk iff (vif.areset_n != 0));
		    	if (vif.t_valid) begin
			    	axi_st_item_collected = axi_st_pkg::axi_st_item#(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE))::type_id::create("axi_st_item_collected");
		    		collect_axi_pkt(axi_st_item_collected);
//			    	if (cfg.print_collected_item) begin
		    			`uvm_info(get_type_name(),$sformatf("Collected item :\n%s", axi_st_item_collected.sprint()), UVM_LOW)
//		    		end
		    		axi_st_item_collected_port.write(axi_st_item_collected);
		    	end
		    end //forever
	    join
  	endtask : collect_transactions


  	task collect_axi_pkt(axi_st_item #(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE)) 	axi_st_item_collected);
	  	int i = 0;
	  	void'(this.begin_tr(axi_st_item_collected));
	  	axi_st_item_collected.data = new[256];
	  	axi_st_item_collected.id 	= vif.t_id;
		do begin
			if (vif.t_valid && vif.t_ready) begin
				axi_st_item_collected.data[i] = vif.t_data;
				i = i+1;
			end
			@(posedge vif.aclk iff (vif.areset_n != 0));
			if (vif.t_last && vif.t_valid && vif.t_ready) begin
				axi_st_item_collected.data[i] = vif.t_data;
				i = i+1;
			end
		end while (!(vif.t_last && vif.t_valid && vif.t_ready));

		axi_st_item_collected.data = new[i](axi_st_item_collected.data);	//reduce size to actual value

		this.end_tr(axi_st_item_collected);
  	endtask : collect_axi_pkt


endclass : mon_c


