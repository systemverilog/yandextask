//------------------------------------------------------------------------------
//
// CLASS: sb_c
//
//------------------------------------------------------------------------------
class sb_c #(
		int DATA_SIZE 	= 32,
		int ID_SIZE 	= 8
	) extends uvm_scoreboard;
  `uvm_analysis_imp_decl(_sent)
  `uvm_analysis_imp_decl(_rcvd)

  cfg_c				cfg;

  uvm_analysis_imp_sent #(axi_st_item#(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE)), sb_c#(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE))) sent_imp;
  uvm_analysis_imp_rcvd #(axi_st_item#(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE)), sb_c#(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE))) rcvd_imp;

  axi_st_item#(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE)) exp_que[$];

  // Provide implementations of virtual methods such as get_type_name and create
  `uvm_component_utils_begin(axi_st_pkg::sb_c#(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE)))
    `uvm_field_object		(cfg, 				UVM_DEFAULT | UVM_REFERENCE)
  `uvm_component_utils_end

  // new - constructor
  function new (string name, uvm_component parent);
    super.new(name, parent);
  endfunction : new

  //build_phase
  function void build_phase(uvm_phase phase);
    sent_imp = new("sent_imp", this);
    rcvd_imp = new("rcvd_imp", this);
  endfunction

  // write_bu_afar
  virtual function void write_sent(axi_st_item#(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE)) trans);
    exp_que.push_back(trans.clone());
    //trans.print();
    //`uvm_info(get_type_name(),$sformatf("write_sent EXP_QUE SIZE = %d", exp_que.size()), UVM_LOW)
//    for (int i=0; i<exp_que.size(); i++) begin
//    	exp_que[i].print();
//    end
  endfunction : write_sent

  // write_bu_afar
  virtual function void write_rcvd(axi_st_item#(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE)) trans);
	axi_st_item#(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE))	exp_trans;
	//trans.print();
	//`uvm_info(get_type_name(),$sformatf("111 EXP_QUE SIZE = %d", exp_que.size()), UVM_LOW)

	if (exp_que.size()) begin
		exp_trans = exp_que.pop_front();

		if (!trans.compare(exp_trans)) begin
			foreach(exp_trans.data[i]) begin
				if (exp_trans.data[i] != trans.data[i]) begin
					`uvm_info(get_type_name(),$sformatf("MISMATCH: expd[%d]: %032h", i, exp_trans.data[i]), UVM_LOW)
					`uvm_info(get_type_name(),$sformatf("MISMATCH: rcvd[%d]: %032h", i, trans.data[i]), UVM_LOW)
				end else begin
					`uvm_info(get_type_name(),$sformatf("expd[%d]: %032h", i, exp_trans.data[i]), UVM_LOW)
					`uvm_info(get_type_name(),$sformatf("rcvd[%d]: %032h", i, trans.data[i]), UVM_LOW)
				end
			end
		end else begin
			`uvm_info(get_type_name(),$sformatf("EQUAL PACKETS!!!"), UVM_LOW)
		end
//		else begin
//			`uvm_info(get_type_name(),$sformatf("TESTTTT!!!"), UVM_LOW)
//			exp_trans.print();
//			trans.print();
//		end
	end else begin
		`uvm_info(get_type_name(),$sformatf("No more data"), UVM_LOW)
	end

  endfunction : write_rcvd

  // report_phase
//  virtual function void report_phase(uvm_phase phase);
//    if(!disable_scoreboard) begin
//      `uvm_info(get_type_name(),
//        $sformatf("Reporting scoreboard information...\n%s", this.sprint()), UVM_LOW)
//    end
//  endfunction : report_phase

endclass : sb_c


