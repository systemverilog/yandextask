virtual class axi_st_base_sequence extends uvm_sequence #(axi_st_pkg::axi_st_item);

  function new(string name="axi_st_base_seq");
     super.new(name);
     set_automatic_phase_objection(1);
  endfunction
  
endclass : axi_st_base_sequence

//------------------------------------------------------------------------------
//
// SEQUENCE: read_byte
//
//------------------------------------------------------------------------------

class write_pkt_seq extends axi_st_base_sequence;

  function new(string name="write_pkt_seq");
    super.new(name);
  endfunction
  
  `uvm_object_utils(write_pkt_seq)
    
  virtual task body();
	  repeat (100) begin
	  		`uvm_do(req)
	  		get_response(rsp);
	  end
  endtask
  
endclass : write_pkt_seq
