`ifndef __AXI_ST_PKG__
`define __AXI_ST_PKG__

`include "uvm_macros.svh"

package axi_st_pkg;
	import uvm_pkg::*;

	//axi
	`include "axi_st_item.sv"
	`include "axi_st_cfg.sv"
	`include "axi_st_drv.sv"
	`include "axi_st_mon.sv"
	`include "axi_st_sb.sv"

	`include "axi_st_agent.sv"

endpackage: axi_st_pkg
`endif

