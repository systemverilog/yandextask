package yandextask_pkg;

	import uvm_pkg::*;

	`include "uvm_macros.svh"

	typedef uvm_config_db#(virtual yandextask_if) yandextask_vif_config;
	typedef virtual yandextask_if yandextask_vif;

	`include "yandextask_macros.sv"
	`include "yandextask_env.sv"

endpackage: yandextask_pkg

