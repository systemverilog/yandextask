`ifndef __YANDEXTASK_IF__
`define __YANDEXTASK_IF__

`include "IAxiStream.sv"

interface yandextask_if #(
	parameter DATA_SIZE = 32,
	parameter ID_SIZE = 8
)();
	
	logic              		clk;
   logic              		reset_n;
	
	IAxiStream #(.DATA_SIZE(DATA_SIZE), .ID_SIZE(ID_SIZE)) 	 in[3:0](.aclk(clk),.areset_n(reset_n)); 
	IAxiStream #(.DATA_SIZE(DATA_SIZE), .ID_SIZE(ID_SIZE)) 	 out(.aclk(clk),.areset_n(reset_n)); 
	
endinterface : yandextask_if
`endif
