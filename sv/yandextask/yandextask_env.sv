//------------------------------------------------------------------------------
//
// CLASS: yandextask_env
//
//------------------------------------------------------------------------------

class yandextask_env #(
		int DATA_SIZE 	= 32,
		int ID_SIZE 	= 8
	) extends uvm_env;

  // Virtual Interface variable
  protected virtual interface yandextask_if 	yandextask_vif;
  
  axi_st_pkg::agent_c				axi_agent[];
  axi_st_pkg::cfg_c					axi_agent_cfg[];	  
  axi_st_pkg::agent_c				axi_agent_out;
  axi_st_pkg::cfg_c					axi_agent_out_cfg;

  // Provide implementations of virtual methods such as get_type_name and create
  `uvm_component_utils_begin(yandextask_env)
//    `uvm_field_int(has_bus_monitor, UVM_DEFAULT)
//    `uvm_field_int(num_masters, UVM_DEFAULT)
//    `uvm_field_int(num_slaves, UVM_DEFAULT)
//    `uvm_field_int(intf_checks_enable, UVM_DEFAULT)
//    `uvm_field_int(intf_coverage_enable, UVM_DEFAULT)
  `uvm_component_utils_end

  // new - constructor
  function new(string name, uvm_component parent);
    super.new(name, parent); 
  endfunction : new

  // build_phase
  function void build_phase(uvm_phase phase);
    string inst_name;
	 string cfg_inst_name; 
//    set_phase_domain("uvm");
    super.build_phase(phase);
     if(!uvm_config_db#(virtual yandextask_if)::get(this, "", "yandextask_vif", yandextask_vif))
       `uvm_fatal("NOVIF",{"virtual interface must be set for: ",get_full_name(),".yandextask_vif"});
    
    
    axi_agent = new[4];
    for(int i = 0; i < 4; i++) begin
      $sformat(inst_name, "axi_agent[%0d]", i);
	   uvm_config_db#(virtual IAxiStream#(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE)))::set(this, inst_name, "vif", yandextask_vif.in[i]);
	   
//	   $sformat(cfg_inst_name, "axi_agent_cfg[%0d]", i);
//	   axi_agent_cfg[i] = axi_st_pkg::cfg_c::type_id::create(cfg_inst_name);
//	   uvm_config_db#(axi_st_pkg::cfg_c)::set(this, inst_name, "cfg", axi_agent_cfg[i]);
	   
      axi_agent[i] = axi_st_pkg::agent_c#(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE))::type_id::create(inst_name, this);
      void'(uvm_config_db#(int)::set(this,{inst_name,".mon"}, 
				 "master_id", i));
      void'(uvm_config_db#(int)::set(this,{inst_name,".drv"}, 
				 "master_id", i));
	   
	   
	   
    end
    
    
    axi_agent_out_cfg = axi_st_pkg::cfg_c::type_id::create("axi_agent_out_cfg");
    axi_agent_out_cfg.has_mon 			= 1;
    axi_agent_out_cfg.has_rdy_handler 	= 1;
	 uvm_config_db#(axi_st_pkg::cfg_c)::set(this, "axi_agent_out", "cfg", axi_agent_out_cfg);
    
    uvm_config_db#(virtual IAxiStream#(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE)))::set(this, "axi_agent_out", "vif", yandextask_vif.out);    
    axi_agent_out = axi_st_pkg::agent_c#(.DATA_SIZE(DATA_SIZE),.ID_SIZE(ID_SIZE))::type_id::create("axi_agent_out", this);
    
  endfunction : build_phase

endclass : yandextask_env


