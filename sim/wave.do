onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/clk
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/reset_n
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/request
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/valid
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/grant
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/next_grant
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/saved_grant
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/src_payload
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/last_cycle
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/packet_in_progress
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/update_grant
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/sink0_payload
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/sink1_payload
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/sink2_payload
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/sink3_payload
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/eop
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/share_0
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/share_1
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/next_grant_share
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/grant_changed
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/first_packet_r
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/first_packet
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/p1_share_count
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/share_count
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/share_count_zero_flag
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/final_packet_0
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/final_packet_1
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/final_packet
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/p1_done
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/first_cycle
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/save_grant
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/prev_request
add wave -noupdate -expand -group arbiter /yandextask_tb_top/arbiter_inst/reset
add wave -noupdate -expand -group round_robin_arb /yandextask_tb_top/arbiter_inst/round_robin_arb_inst/clk
add wave -noupdate -expand -group round_robin_arb /yandextask_tb_top/arbiter_inst/round_robin_arb_inst/reset
add wave -noupdate -expand -group round_robin_arb /yandextask_tb_top/arbiter_inst/round_robin_arb_inst/request
add wave -noupdate -expand -group round_robin_arb /yandextask_tb_top/arbiter_inst/round_robin_arb_inst/grant
add wave -noupdate -expand -group round_robin_arb /yandextask_tb_top/arbiter_inst/round_robin_arb_inst/increment_top_priority
add wave -noupdate -expand -group round_robin_arb /yandextask_tb_top/arbiter_inst/round_robin_arb_inst/save_top_priority
add wave -noupdate -expand -group round_robin_arb /yandextask_tb_top/arbiter_inst/round_robin_arb_inst/top_priority_reg
add wave -noupdate -expand -group round_robin_arb /yandextask_tb_top/arbiter_inst/round_robin_arb_inst/last_grant
add wave -noupdate -expand -group round_robin_arb /yandextask_tb_top/arbiter_inst/round_robin_arb_inst/result
add wave -noupdate -expand -group round_robin_arb /yandextask_tb_top/arbiter_inst/round_robin_arb_inst/grant_double_vector
add wave -noupdate -expand -group in_0 /yandextask_tb_top/arbiter_inst/in_0/aclk
add wave -noupdate -expand -group in_0 /yandextask_tb_top/arbiter_inst/in_0/areset_n
add wave -noupdate -expand -group in_0 /yandextask_tb_top/arbiter_inst/in_0/t_valid
add wave -noupdate -expand -group in_0 /yandextask_tb_top/arbiter_inst/in_0/t_ready
add wave -noupdate -expand -group in_0 /yandextask_tb_top/arbiter_inst/in_0/t_last
add wave -noupdate -expand -group in_0 /yandextask_tb_top/arbiter_inst/in_0/t_data
add wave -noupdate -expand -group in_0 /yandextask_tb_top/arbiter_inst/in_0/t_id
add wave -noupdate -expand -group in_1 /yandextask_tb_top/arbiter_inst/in_1/aclk
add wave -noupdate -expand -group in_1 /yandextask_tb_top/arbiter_inst/in_1/areset_n
add wave -noupdate -expand -group in_1 /yandextask_tb_top/arbiter_inst/in_1/t_valid
add wave -noupdate -expand -group in_1 /yandextask_tb_top/arbiter_inst/in_1/t_ready
add wave -noupdate -expand -group in_1 /yandextask_tb_top/arbiter_inst/in_1/t_last
add wave -noupdate -expand -group in_1 /yandextask_tb_top/arbiter_inst/in_1/t_data
add wave -noupdate -expand -group in_1 /yandextask_tb_top/arbiter_inst/in_1/t_id
add wave -noupdate -expand -group in_2 /yandextask_tb_top/arbiter_inst/in_2/aclk
add wave -noupdate -expand -group in_2 /yandextask_tb_top/arbiter_inst/in_2/areset_n
add wave -noupdate -expand -group in_2 /yandextask_tb_top/arbiter_inst/in_2/t_valid
add wave -noupdate -expand -group in_2 /yandextask_tb_top/arbiter_inst/in_2/t_ready
add wave -noupdate -expand -group in_2 /yandextask_tb_top/arbiter_inst/in_2/t_last
add wave -noupdate -expand -group in_2 /yandextask_tb_top/arbiter_inst/in_2/t_data
add wave -noupdate -expand -group in_2 /yandextask_tb_top/arbiter_inst/in_2/t_id
add wave -noupdate -expand -group in_3 /yandextask_tb_top/arbiter_inst/in_3/aclk
add wave -noupdate -expand -group in_3 /yandextask_tb_top/arbiter_inst/in_3/areset_n
add wave -noupdate -expand -group in_3 /yandextask_tb_top/arbiter_inst/in_3/t_valid
add wave -noupdate -expand -group in_3 /yandextask_tb_top/arbiter_inst/in_3/t_ready
add wave -noupdate -expand -group in_3 /yandextask_tb_top/arbiter_inst/in_3/t_last
add wave -noupdate -expand -group in_3 /yandextask_tb_top/arbiter_inst/in_3/t_data
add wave -noupdate -expand -group in_3 /yandextask_tb_top/arbiter_inst/in_3/t_id
add wave -noupdate -expand -group out /yandextask_tb_top/arbiter_inst/out/aclk
add wave -noupdate -expand -group out /yandextask_tb_top/arbiter_inst/out/areset_n
add wave -noupdate -expand -group out /yandextask_tb_top/arbiter_inst/out/t_valid
add wave -noupdate -expand -group out /yandextask_tb_top/arbiter_inst/out/t_ready
add wave -noupdate -expand -group out /yandextask_tb_top/arbiter_inst/out/t_last
add wave -noupdate -expand -group out /yandextask_tb_top/arbiter_inst/out/t_data
add wave -noupdate -expand -group out /yandextask_tb_top/arbiter_inst/out/t_id
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {25439 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 455
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {25382 ns}
