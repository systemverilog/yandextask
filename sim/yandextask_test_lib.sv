`include "yandextask_example_tb.sv"


// Base Test
class yandextask_example_base_test extends uvm_test;

  `uvm_component_utils(yandextask_example_base_test)

  yandextask_example_tb yandextask_example_tb0;
  uvm_table_printer printer;
  bit test_pass = 1;

  function new(string name = "yandextask_example_base_test", 
    uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    // Enable transaction recording for everything
    uvm_config_db#(int)::set(this, "*", "recording_detail", UVM_FULL);
    // Create the tb
    yandextask_example_tb0 = yandextask_example_tb::type_id::create("yandextask_example_tb0", this);
    // Create a specific depth printer for printing the created topology
    printer = new();
    printer.knobs.depth = 3;
  endfunction : build_phase

  function void end_of_elaboration_phase(uvm_phase phase);
    // Set verbosity for the bus monitor for this demo
//     if(yandextask_example_tb0.yandextask0.bus_monitor != null)
//       yandextask_example_tb0.yandextask0.bus_monitor.set_report_verbosity_level(UVM_FULL);
    `uvm_info(get_type_name(),
      $sformatf("Printing the test topology :\n%s", this.sprint(printer)), UVM_LOW)
  endfunction : end_of_elaboration_phase

  task run_phase(uvm_phase phase);
    //set a drain-time for the environment if desired
    phase.phase_done.set_drain_time(this, 50);
  endtask : run_phase

  function void extract_phase(uvm_phase phase);
//    if(yandextask_example_tb0.scoreboard0.sbd_error)
//      test_pass = 1'b0;
  endfunction // void
  
  function void report_phase(uvm_phase phase);
    if(test_pass) begin
      `uvm_info(get_type_name(), "** UVM TEST PASSED **", UVM_NONE)
    end
    else begin
      `uvm_error(get_type_name(), "** UVM TEST FAIL **")
    end
    $stop;
  endfunction

endclass : yandextask_example_base_test


// test1
class test1 extends yandextask_example_base_test;

  `uvm_component_utils(test1)

  function new(string name = "test1", uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  virtual function void build_phase(uvm_phase phase);
  begin
    uvm_config_db#(uvm_object_wrapper)::set(this,
		    "yandextask_example_tb0.yandextask0.axi_agent[0].sqr.run_phase", 
			       "default_sequence",
				write_pkt_seq::type_id::get());
	 uvm_config_db#(uvm_object_wrapper)::set(this,
		    "yandextask_example_tb0.yandextask0.axi_agent[1].sqr.run_phase", 
			       "default_sequence",
				write_pkt_seq::type_id::get());
	 uvm_config_db#(uvm_object_wrapper)::set(this,
		    "yandextask_example_tb0.yandextask0.axi_agent[2].sqr.run_phase", 
			       "default_sequence",
				write_pkt_seq::type_id::get());
	 uvm_config_db#(uvm_object_wrapper)::set(this,
		    "yandextask_example_tb0.yandextask0.axi_agent[3].sqr.run_phase", 
			       "default_sequence",
				write_pkt_seq::type_id::get()); 
    super.build_phase(phase);
  end
  endfunction : build_phase

endclass : test1

