#!/usr/bin/tclsh
set QUESTA_HOME 						"C:/questasim64_10.4a/"
set UVM_DPI_HOME 						${QUESTA_HOME}/uvm-1.2/win64/						
set USES_PRECOMPILED_UVM_DPI 			1

set UVM_HOME							${QUESTA_HOME}uvm-1.2		
set INCDIR_DEF 							"+incdir+${UVM_HOME}/src+../rtl+../sv+../sv/yandextask+../sv/axi"
set VLOG_CMD							"vlog +acc -L ${QUESTA_HOME}uvm-1.2 ${INCDIR_DEF}"
set USES_DPI 							1
set UVM_TESTNAME 						test1

proc comp {} {
	global VLOG_CMD
	
	eval exec vlib work
	eval ${VLOG_CMD} ../sv/axi/axi_st_pkg.sv
	eval ${VLOG_CMD} ../sv/yandextask/yandextask_pkg.sv
	eval ${VLOG_CMD} ../rtl/round_robin_arb.sv
	eval ${VLOG_CMD} ../rtl/arbiter.sv
	
	eval ${VLOG_CMD} yandextask_tb_top.sv
	
	vopt -L work yandextask_tb_top -o yandextask_tb_top_opt
}

proc runnn {} {
	global VSIM
	global UVM_TESTNAME
	global QUESTA_HOME
	global UVM_DPI_HOME
	
	comp
	eval vsim -L ${QUESTA_HOME}uvm-1.2 -assertdebug -voptargs="+acc" +UVM_TESTNAME=${UVM_TESTNAME} -sv_lib ${UVM_DPI_HOME}uvm_dpi +UVM_CONFIG_DB_TRACE -l run.log yandextask_tb_top_opt
#	add wave -position insertpoint sim:/yandextask_tb_top/arbiter_inst/*
}

proc all {} {
	runnn
}
