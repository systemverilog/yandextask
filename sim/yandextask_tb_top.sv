`include "yandextask_pkg.sv"
`include "arbiter.sv"
`include "yandextask_if.sv"

module yandextask_tb_top;
  import uvm_pkg::*;
  import yandextask_pkg::*;
  `include "yandextask_test_lib.sv"

  yandextask_if #(.DATA_SIZE(`DATA_SIZE), .ID_SIZE(`ID_SIZE)) 	 yandextask_vif(); 
  
  
  arbiter #(
	.DATA_SIZE 	(`DATA_SIZE),
	.ID_SIZE 	(`ID_SIZE)
	) arbiter_inst (
	.clk			(yandextask_vif.clk),
  	.reset_n		(yandextask_vif.reset_n),
  	.in_0			(yandextask_vif.in[0]),
  	.in_1			(yandextask_vif.in[1]),
  	.in_2			(yandextask_vif.in[2]),
  	.in_3			(yandextask_vif.in[3]),
  	.out			(yandextask_vif.out)
);
	
//	assign yandextask_vif.out.t_ready = 1'b1;
	
  initial begin automatic uvm_coreservice_t cs_ = uvm_coreservice_t::get();
    uvm_config_db#(virtual yandextask_if)::set(cs_.get_root(), "*", "yandextask_vif", yandextask_vif);
    run_test();
  end

  initial begin
    yandextask_vif.reset_n <= 1'b0;
    yandextask_vif.clk <= 1'b1;
    #51 yandextask_vif.reset_n = 1'b1;
  end

  //Generate Clock
  always
    #5 yandextask_vif.clk = ~yandextask_vif.clk;

endmodule
