`include "axi_st_seq_lib.sv"

//------------------------------------------------------------------------------
//
// CLASS: yandextask_example_tb
//
//------------------------------------------------------------------------------

class yandextask_example_tb extends uvm_env;

  // Provide implementations of virtual methods such as get_type_name and create
  `uvm_component_utils(yandextask_example_tb)

  // yandextask environment
  yandextask_env #(.DATA_SIZE(`DATA_SIZE), .ID_SIZE(`ID_SIZE)) yandextask0;

  // Scoreboard to check the memory operation of the slave.
//  yandextask_example_scoreboard scoreboard0;

  // new
  function new (string name, uvm_component parent=null);
    super.new(name, parent);
  endfunction : new

  // build_phase
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
//    uvm_config_db#(int)::set(this,"yandextask0", 
//			       "num_masters", 1);
//    uvm_config_db#(int)::set(this,"yandextask0", 
//			       "num_slaves", 1);
    
    yandextask0 = yandextask_env#(.DATA_SIZE(`DATA_SIZE), .ID_SIZE(`ID_SIZE))::type_id::create("yandextask0", this);
//    scoreboard0 = yandextask_example_scoreboard::type_id::create("scoreboard0", this);
  endfunction : build_phase

  function void connect_phase(uvm_phase phase);
    // Connect slave0 monitor to scoreboard
//    yandextask0.slaves[0].monitor.item_collected_port.connect(
//      scoreboard0.item_collected_export);
  endfunction : connect_phase

  function void end_of_elaboration_phase(uvm_phase phase);
    // Set up slave address map for yandextask0 (basic default)
//    yandextask0.set_slave_address_map("slaves[0]", 0, 16'hffff);
  endfunction : end_of_elaboration_phase

endclass : yandextask_example_tb


