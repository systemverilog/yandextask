`include "yandextask_fpga_defines.sv"

module yandextask_fpga_top (
	input logic 			clk,
  	input logic 			reset_n,
  	IAxiStream.Slave 		in_0,
  	IAxiStream.Slave 		in_1,
  	IAxiStream.Slave 		in_2,
  	IAxiStream.Slave 		in_3,
  	IAxiStream.Master 	out
);
  
  	arbiter #(
		.DATA_SIZE 	(`DATA_SIZE),
		.ID_SIZE 	(`ID_SIZE)
		) arbiter_inst (
		.clk			(clk),
	  	.reset_n		(reset_n),
	  	.in_0			(in_0),
	  	.in_1			(in_1),
	  	.in_2			(in_2),
	  	.in_3			(in_3),
	  	.out			(out)
	);

endmodule
