`timescale 1 ns / 1 ns

module arbiter #(
	parameter DATA_SIZE = 32,
	parameter ID_SIZE = 8
)(
	input logic clk,
	input logic reset_n,
	IAxiStream.Slave in_0,
	IAxiStream.Slave in_1,
	IAxiStream.Slave in_2,
	IAxiStream.Slave in_3,
	IAxiStream.Master out
);

// ------------------------------------------
// Internal Parameters
// ------------------------------------------
	localparam PAYLOAD_W        = DATA_SIZE + ID_SIZE + 1;
	localparam NUM_INPUTS       = 4;

// ------------------------------------------
// Internal Signals
// ------------------------------------------
	logic [NUM_INPUTS-1 : 0] 			request;
	logic [NUM_INPUTS-1 : 0] 			valid;
	logic [NUM_INPUTS-1 : 0] 			grant;
	logic [PAYLOAD_W-1 : 0]  			out_payload;
	logic                      		last_cycle;
	logic                       		packet_in_progress;

	logic [PAYLOAD_W-1 : 0]  			in_0_payload;
	logic [PAYLOAD_W-1 : 0]  			in_1_payload;
	logic [PAYLOAD_W-1 : 0]  			in_2_payload;
	logic [PAYLOAD_W-1 : 0]  			in_3_payload;
	logic [NUM_INPUTS-1 : 0] 			eop;
	
	logic [NUM_INPUTS-1 : 0] 			prev_request;
// ------------------------------------------
// Code
// ------------------------------------------
	assign valid[0] = in_0.t_valid;
	assign valid[1] = in_1.t_valid;
	assign valid[2] = in_2.t_valid;
	assign valid[3] = in_3.t_valid;

	assign eop[0]   = in_0.t_last;
	assign eop[1]   = in_1.t_last;
	assign eop[2]   = in_2.t_last;
	assign eop[3]   = in_3.t_last;

	assign last_cycle = out.t_valid & out.t_ready & out.t_last;

	always_ff @(posedge clk or negedge reset_n) begin
		if (!reset_n) begin
			packet_in_progress <= 1'b0;
		end else begin
			if (last_cycle)
				packet_in_progress <= 1'b0;
			else if (out.t_valid)
				packet_in_progress <= 1'b1;
		end
	end

	
	always_ff @(posedge clk or negedge reset_n) begin
		if (!reset_n)
			prev_request <= '0;
		else
			prev_request <= request & ~(valid & eop);
	end

	assign request = (prev_request | valid);


	round_robin_arb
	#(
		.NUM_REQUESTERS(NUM_INPUTS)
	) round_robin_arb_inst (
		.clk                    (clk),
		.reset_n                (reset_n),
		.request                (request),
		.grant                  (grant),
		.save_top_priority      (out.t_valid),
		.increment_top_priority (last_cycle)
	);

	// ------------------------------------------
	// Output logic
	// ------------------------------------------
	assign in_0.t_ready = out.t_ready & grant[0];
	assign in_1.t_ready = out.t_ready & grant[1];
	assign in_2.t_ready = out.t_ready & grant[2];
	assign in_3.t_ready = out.t_ready & grant[3];

	assign out.t_valid = |(grant & valid);

	assign in_0_payload = {in_0.t_data, in_0.t_id, in_0.t_last};
	assign in_1_payload = {in_1.t_data, in_1.t_id, in_1.t_last};
	assign in_2_payload = {in_2.t_data, in_2.t_id, in_2.t_last};
	assign in_3_payload = {in_3.t_data, in_3.t_id, in_3.t_last};

	always_comb begin
		out_payload =
			in_0_payload & {PAYLOAD_W {grant[0]} } |
			in_1_payload & {PAYLOAD_W {grant[1]} } |
			in_2_payload & {PAYLOAD_W {grant[2]} } |
			in_3_payload & {PAYLOAD_W {grant[3]} };
	end

	assign {out.t_data, out.t_id, out.t_last} = out_payload;
endmodule



