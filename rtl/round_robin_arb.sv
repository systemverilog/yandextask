`timescale 1 ns / 1 ns

module round_robin_arb
#(
    parameter NUM_REQUESTERS = 4
)
(
    input clk,
    input reset_n,
   
    input [NUM_REQUESTERS-1:0]  request,
    output [NUM_REQUESTERS-1:0] grant,

    input                       increment_top_priority,
    input                       save_top_priority
);

    // --------------------------------------
    // Signals
    // --------------------------------------
    reg  [NUM_REQUESTERS-1:0]   top_priority_reg;
    wire [2*NUM_REQUESTERS-1:0] result;

    // --------------------------------------
    // Decision Logic
    // --------------------------------------
    round_robin_arb_adder
    #(
        .WIDTH (2 * NUM_REQUESTERS)
    ) 
    adder
    (
        .a ({ ~request, ~request }),
        .b ({{NUM_REQUESTERS{1'b0}}, top_priority_reg}),
        .sum (result)
    );

  
    // Do the math in double-vector domain
     wire [2*NUM_REQUESTERS-1:0] grant_double_vector;
     assign grant_double_vector = {request, request} & result;

     // --------------------------------------
     // Extract grant from the top and bottom halves
     // of the double vector.
     // --------------------------------------
     assign grant =
         grant_double_vector[NUM_REQUESTERS - 1 : 0] |
         grant_double_vector[2 * NUM_REQUESTERS - 1 : NUM_REQUESTERS];

    // --------------------------------------
    // Left-rotate the last grant vector to create top_priority.
    // --------------------------------------
    always @(posedge clk or negedge reset_n) begin
        if (!reset_n) begin
            top_priority_reg <= 1'b1;
        end
        else begin
            if (increment_top_priority) begin
                 if (|request)
                     top_priority_reg <= { grant[NUM_REQUESTERS-2:0],
                         grant[NUM_REQUESTERS-1] };
                 else
                     top_priority_reg <= { top_priority_reg[NUM_REQUESTERS-2:0], top_priority_reg[NUM_REQUESTERS-1] };
             end
             else if (save_top_priority) begin
                 top_priority_reg <= grant; 
             end
        end
    end

endmodule


module round_robin_arb_adder
#(
    parameter WIDTH = 8
)
(
    input [WIDTH-1:0] a,
    input [WIDTH-1:0] b,

    output [WIDTH-1:0] sum
);

    wire [WIDTH:0] sum_lint;

    genvar i;
    generate if (WIDTH <= 8) begin : full_adder

        wire cout[WIDTH-1:0];

        assign sum[0]  = (a[0] ^ b[0]);
        assign cout[0] = (a[0] & b[0]);

        for (i = 1; i < WIDTH; i = i+1) begin : arb

            assign sum[i] = (a[i] ^ b[i]) ^ cout[i-1];
            assign cout[i] = (a[i] & b[i]) | (cout[i-1] & (a[i] ^ b[i]));

        end

    end else begin : carry_chain

        assign sum_lint = a + b;
        assign sum = sum_lint[WIDTH-1:0];

    end
    endgenerate

endmodule
